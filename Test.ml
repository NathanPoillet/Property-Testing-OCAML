#use "Generator.ml" ;;
#use "Reduction.ml" ;;
#use "Property.ml" ;;

module Test :
  sig
    (** Type d'un test portant sur des éléments de type 'a *)
    type 'a t
    
    (** Construit un test
      * @param gen  générateur pseudo-aléatoire de valeurs de test
      * @param red  stratégie de réduction
      * @param name nom du test
      * @param prop propriété qui fait l'objet du test
      * @return     test créé
      *)
    val make_test : 'a Generator.t -> 'a Reduction.t -> string -> 'a Property.t -> 'a t
    
    (** Effectue un test
      * @param n    nombre de valeurs à tester
      * @param test test à effectuer
      * @return     `true` si n > 0 et que toutes les valeurs à tester satisfont les conditions
      *)
    val check : int -> 'a t -> bool
    
    (** Cherche une valeur simple ne vérifiant pas la propriété
      * @param n nombre de valeurs à tester
      * @return  `None` si toutes les valeurs de test générées par `gen` vérifient `prop`,
                 une valeur ne vérifiant pas `prop` (éventuellement en appliquant `red`) sinon
      *)
    val fails_at : int -> 'a t -> 'a option

    
    (** Exécute plusieurs tests
      * @param n     nombre de valeurs testées par test
      * @param tests liste des tests à vérifier
      * @return      tableau associatif des résultats
      *)
      
      
    val execute : int -> ('a t) list -> ('a t * 'a option) list
    
  end =
  struct
    type 'a t = {
      gen : 'a Generator.t;
      red : 'a Reduction.t;
      name : string;
      prop : 'a Property.t;
    }

    let make_test gen red name prop = {
      gen = gen;
      red = red;
      name = name;
      prop = prop;
    }

    let check n test = 
      let rec verif n = 
        if n < 0 then false
        else if n = 0 then true
        else if test.prop (Generator.next test.gen) then verif (n-1)
        else false
      in verif n

    let fails_at n test =
      let rec verif n = 
        if n < 0 then None
        else if n = 0 then None
        else let x = Generator.next test.gen in
          if test.prop x then verif (n-1)
          else let falseReduit = (List.fold_left (fun acc e -> if test.prop e then acc else acc@[e]) [] (test.red x)) 
          in if List.length falseReduit = 0 then Some x else Some (List.nth falseReduit 0)
      in verif n


    let execute n tests =
      let rec aux n tests acc = 
        if n < 0 then acc
        else if n = 0 then acc
        else let res = List.map (fun e -> (e, fails_at 100 e)) tests in
          aux (n-1) tests (acc@res)
      in aux n tests []
    
    
  end ;;

let test = Test.make_test (Generator.int 1 100) (Reduction.int_nonneg) "test1" (fun x -> x < 10) in Test.execute 20 [test];;

