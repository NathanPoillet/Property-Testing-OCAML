include Random;;

module Generator :
  sig
    (** Type du générateur pseudo-aléatoire de données de type 'a *)
    type 'a t
    
    (** Renvoie une nouvelle valeur aléatoire
      * @param gen générateur pseudo-aléatoire
      * @return    nouvelle valeur aléatoire en utilisant `gen`
      *)
    val next : 'a t -> 'a


    (** Générateur constant
      * @param x valeur
      * @return  générateur de l'unique valeur `x`
      *)
    val const : 'a -> 'a t

    (* GENERATEURS DE TYPES DE BASE *)
 
    (** Générateur pseudo-aléatoire de booléens
      * @param prob probabilité de la valeur `true`
      * @return     générateur pseudo-aléatoire de valeurs booléennes
      *)
    val bool : float -> bool t

    (** Générateur pseudo-aléatoire d'entiers
      * @param a borne inférieure
      * @param b borne supérieure
      * @return  générateur pseudo-aléatoire de valeurs entières entre `a` et `b` inclus
      *)
    val int : int -> int -> int   t

    (** Générateur pseudo-aléatoire d'entiers positifs ou nuls
      * @param n borne supérieure
      * @return  générateur pseudo-aléatoire de valeurs entières entre 0 et `n` inclus
      *)
    val int_nonneg : int -> int   t

    (** Générateur pseudo-aléatoire de flottants
      * @param x borne inférieure
      * @param y borne supérieure
      * @return  générateur pseudo-aléatoire de valeurs flottantes entre `x` et `y` inclus
      *)
    val float : float -> float -> float t

    (** Générateur pseudo-aléatoire de flottants positifs ou nuls
      * @param x borne supérieure
      * @return  générateur pseudo-aléatoire de valeurs flottantes entre 0 et `x` inclus
      *)
    val float_nonneg : float -> float t

    (** Générateur pseudo-aléatoire de caractères *)
    val char : char t

    (** Générateur pseudo-aléatoire de caractères alphanumériques *)
    val alphanum : char t

    (* GENERATEURS DE CHAINE DE CARACTERE *)
    (** Générateur de chaînes de caractères
      * @param n   longueur maximale de la chaîne de caractère
      * @param gen générateur pseudo-aléatoire de caractères
      * @return    générateur pseudo-aléatoire de chaînes de caractères dont chaque caractéré est généré avec `gen`
      *)
    val string : int -> char t -> string t

    (* GENERATEURS DE LISTES *)

    (** Générateur de listes
      * @param n   longueur maximale de la liste
      * @param gen générateur pseudo-aléatoire d'éléments
      * @return    générateur pseudo-aléatoire de listes dont chaque élément est généré avec `gen`
      *)
    val list : int -> 'a t -> ('a list) t
    
    (** Générateur de graphs
      * @param a   nombre de sommets
      * @param n 	 nombre d'arêtes
      * @return    générateur pseudo-aléatoire de graphs dont chaque élément est généré avec `gen`
      *)
    val graph : int -> int ->  (int*int*int) list t
 
    (* TRANSFORMATIONS *)

    (** Générateur pseudo-aléatoire de couples
      * @param fst_gen générateur pseudo-aléatoire de la première coordonnée
      * @param snd_gen générateur pseudo-aléatoire de la deuxième coordonnée
      * @return        générateur pseudo-aléatoire du couple
      *)
   
   
   val combine : 'a t -> 'b t -> ('a * 'b) t

    (** Applique un post-traitement à  un générateur pseudo-aléatoire
      * @param f   post-traitement à  appliquer à  chaque valeur générée
      * @param gen générateur pseudo-aléatoire
      * @return    générateur pseudo-aléatoire obtenu en appliquant `f` à  chaque valeur générée par `gen`
      *)
    val map : ('a -> 'b) -> 'a t -> 'b t
    
    (** Applique un filtre à  un générateur pseudo-aléatoire
      * @param p   filtre à  appliquer à  chaque valeur générée
      * @param gen générateur pseudo-aléatoire
      * @return    générateur pseudo-aléatoire ne générant des valeurs de `gen` que si elles vérifient `p`
      *)
    val filter : ('a -> bool) -> 'a t -> 'a t

    (** Applique un post-traitement dépendant d'un filtre à  un générateur pseudo-aléatoire
      * @param p   filtre à  appliquer à  chaque valeur générée
      * @param f   couple des post-traitements à  appliquer à  chaque valeur générée
      * @param gen générateur pseudo-aléatoire
      * @return    générateur pseudo-aléatoire obtenu en appliquant `fst f` pour toute valeur vérifiant `p`
      *                                                          et `snd f` pour toute valeur ne le vérifiant pas
      *)
    val partitioned_map : ('a -> bool) -> (('a -> 'b) * ('a -> 'b)) -> 'a t -> 'b t
    
  end =
  struct


(*============================================================================================*)
(*==================================      Type & Divers      =================================*)
(*============================================================================================*)



    (*Le type : 
    - une constante de type 'a
    - un générateur Arg d'un type simple composé d'une liste d'entier correspondant aux arguments de ce type (dont le premier est juste l'index du type), ainsi que la fonction next associé à ce type (int list->'a)
    - un générateur de liste qui contient en entier (la taille), un élément (obligatoire du àla définition la fonction list, cet élément sera donc de type 'a list t), la liste des arguments et la fonction next associé à la liste
    *)
    type  'a t = 
    |Const of 'a 
    |Arg of int list * (int list -> 'a)
    |List of int * 'a  * int list * ( int -> int list -> 'a)
    

	(* Initialiser la seed random*)
    let _ = Random.self_init()


    (*Renvoie la precision du float entré en parametre. Ex : precision 1.001 = 3 *)
    let precision x = let s = string_of_float x in
                      let rec aux i = match s.[i] with 
                      | c when c = '.' -> i 
                      |_ -> aux (i+1) 
                      in
                      String.length s - aux 0 - 1
                      
    (*Renvoie la longueur du nombre x*)
    let length_number x = let s = string_of_int x in String.length s

	(* Renvoie le flottant ex.rx *)
	let rec calculer_borne ex rx acc = 
		 match rx with
		 |rx when  (float_of_int rx) <=1. -> float_of_int ex +. acc 
		 |rx -> calculer_borne ex (rx/10) (acc +. (float_of_int (rx mod 10)) /. (10. ** (float_of_int (length_number rx))))
			 



(*============================================================================================*)
(*=================================      Fonction Next      ==================================*)
(*============================================================================================*)

	 (* Génération de constante*)
    let nextConst gen = match gen with
    |Const x -> x
    | _ -> failwith "next"


    (*Generation aléatoire d'un bool (prob: p * 10 car on ne peut mettre que des entiers dans la liste)*)
    let nextBool gen = match gen with
    |  [1;prob] when Random.float 1. < (float_of_int prob) /. ((float_of_int(length_number  prob))  *. 10.) ->  false 
    |  [1;prob] ->  true
    | _ -> failwith "next"
    
	 (*Generation aléatoire d'un entier entre a et b*)
    let nextInt gen = match gen with
    | [2;a;b] -> ((Random.int (b-a)) + a)
    | _ -> failwith "nextInt"


    (*ex : partie entiere de la borne inferieure
      rx : chiffres apres la virgule de la borne inferieure en entier
      ey : partie entiere de la borne superieure
      ry : chiffres apres la virgule de la borne superieure en entier*)
    let nextFloat gen = match gen with
    | [3;ex;rx;ey;ry] -> let borne_inf = calculer_borne ex rx 0. in
                         let borne_sup = calculer_borne ey ry 0. in
                          ((Random.float (borne_sup -. borne_inf)) +. borne_inf)
    | _ -> failwith "nextFloat"
    
    (* Generation aleatoire d'un char. Si alphanum=1, on ne renvoie que des charactères alphanumérique, sinon tout type de caractères*)
    let nextChar gen = match gen with
    |  [4;alphanum] when alphanum = 1 -> let c = Random.int 62 in let f c =  match c with 
														|c when c<10 -> char_of_int (c + 48)  (*Charactere 0->9 aléatoire*)
														|c when c<36 ->char_of_int (c + 55)  (*Charactere A->Z aléatoire*)
														|c -> char_of_int (c + 61)(*Charactere a->z aléatoire*) in f c
                                         
    |  [4;alphanum] ->  (char_of_int (Random.int 256)) (*Charactere aléatoire*)
    | _ -> failwith "nextChar"

	(*Creation d'un string à partir de gen*)
    let rec stringNext gen str =  match gen with
    |  [5;0;_]-> str
    |  [5;n;alphanum] -> stringNext ( [5;n-1;alphanum]) (str^(String.make 1 ( (nextChar ( [4;alphanum])))))
    | _ -> failwith "stringNext"


    (*Generation de string*)
    let nextString gen = match gen with
    |  [5;_;_] ->  (stringNext gen "")
    | _ -> failwith "nextString"
    
    (*Generation d'un liste. UneListe n'est ici que pour que le type soit correct mais est complètement inutile. gen est de type 'a t, et n est le nombre d'éléments de la liste*)
    let rec nextList gen n uneList = match n,gen with
    |0,_ -> []
    |n, Const c -> c:: (nextList gen (n-1) uneList )
    |n, Arg (x,y) -> (y x)::(nextList gen (n-1) uneList)
    |n, List (m,list,x,y) -> (y m x)::(nextList gen (n-1) uneList)
	
	(*Generation de n arête sur le graphe g*)
   let rec areteNext g a n = match n with
    |0 -> g
    |n -> areteNext ([((Random.int a),(Random.int a),(Random.int a))] @ g) a (n-1) 
    
    (*Generation de graph*)
	let nextGraph gen = match gen with
	|[6;a;n] -> areteNext [] a n
	
	(*Fonction next*)
    let next gen =  match gen with
    |Const c ->c
    |Arg (a,f)->f a
    |List (n,list,x,y) -> y n x
	
	
	
(*============================================================================================*)
(*=============================      Fonction de Génération      =============================*)
(*============================================================================================*)
	
    
    


    (* Création de generateur de constante*)
	 let const x = Const x
    
	 
	 (* Création de generateur d'entier*)
    let int a b = Arg ([2;a;b], nextInt)
    
    (* Création de generateur d'entier non négatif*)
    let int_nonneg n = int 0 n
    
    (* Création de generateur de booleen avec la probabilité prob*)
    let bool prob = Arg ( [1;int_of_float (prob *.  (10. ** float_of_int (precision (prob))) )], nextBool )
    

    (*Creation de generateur de flottant : tableau contenant la partie entiere de x, les chiffres apres la virgule de x en entier, partie entiere de y, les chiffres apres la virgule de y en entier*)
	 let float x y = 
		 let listStringFloat_x = String.split_on_char '.' (Float.to_string x) in 
		 let listStringFloat_y = String.split_on_char '.' (Float.to_string y) in
			 Arg ([3;int_of_string (List.nth listStringFloat_x 0);
			  int_of_string (List.nth listStringFloat_x 1);
			  int_of_string (List.nth listStringFloat_y 0);
			  int_of_string (List.nth listStringFloat_y 1)],nextFloat)
    
    (*creation de generateur non negatif*)
    let float_nonneg x = float 0. x
    
    (*creation de generateur de caractère*)
    let char = Arg ([4;0], nextChar )
    
    (*creation de generateur de caractère alphanumérique*)
    let alphanum = Arg ([4;1], nextChar)
    
    (*creation de generateur de string*)
    let string n gen = match gen with 
    | Arg ([x;alphanum],_) -> Arg ([5;n;alphanum],nextString)
    | _ -> failwith "string"

    (*fonction revoyant une liste possédant un élément du type de gen (afin que le type de la fonction list soit correct)*)
    let rec listRec n gen res = match n,gen with
    |0,_ -> res
    |n,(Const c) ->    [c]
    |n,Arg (x,y) ->  [y x]
    |n, List (m,list,x,y) -> [y m x]
    
    (*creation de generateur de liste de taille n à partir de gen*)
    let list n gen = let res2 = listRec n gen [] in  match (List.hd res2) ,gen with
    |_, Const c -> List (n,res2,[0],(nextList gen))
    |_, Arg (x,y) -> List (n,res2,x,(nextList gen))
    |_, List (m,list,x,y) -> List (n,res2,x,(nextList gen))
	 
	 (*creation de generateur de graph orienté avec a sommets et n arêtes*)
	 let graph a n = Arg ([6;a;n], nextGraph)
(*============================================================================================*)
(*================================ Fonctions de transformation ===============================*)
(*============================================================================================*)
   

   let combine fst_gen snd_gen = match fst_gen,snd_gen with
    |Const a, Const b -> Const (a,b)
    |Const a, Arg (x,y) -> Arg (x,(fun x -> (a,y x)))
    |Const a, List (n,list,x,y) -> Arg  (x,(fun x -> (a,y n x)))
    |Arg (x,y), Const b -> Arg (x,(fun x -> (y x,b)))
    |Arg (x1,y1), Arg (x2,y2) -> Arg (x1,(fun x -> (y1 x1,y2 x2)))
    |Arg (x1,y1), List (n,list,x,y) -> Arg  (x1,(fun x -> (y1 x1,y n x)))
    |List (n,list,x,y), Const a -> Arg  (x,(fun x -> (y n x,a)))
    |List (n,list,x,y), Arg (x1,y1) -> Arg  (x,(fun x -> (y n x,y1 x1)))
    |List (n,list,x,y), List (m,list1,x1,y1) -> Arg  (x,(fun x -> (y n x,y1 m x1)))
    
    let map f gen = match gen with
    |Const a -> Const (f a)
    |Arg (x,y) -> Arg (x,(fun x -> f (y x)))
    |List (n,list,x,y) -> Arg(x,(fun x -> f (y n x)))

	 (*Effectue la fonction next d'un type Arg jusqu'à ce que le propriété p soit respecté *)
    let rec nextUntilCorrectArg p y x = match y x with
      |z when p z -> z
      |z -> nextUntilCorrectArg p y x
      
    (*Effectue la fonction next d'un type List jusqu'à ce que le propriété p soit respecté *)
    let rec nextUntilCorrectList p y n x = match y n x with
      |z when p z -> z
      |z -> nextUntilCorrectList p y n x
      

    let filter p gen =  
      match gen with
      |Const a when p a -> Const a 
      |Arg (x,y) -> Arg (x,(fun x -> nextUntilCorrectArg p y x ))
      |List (n,list1,x,y) -> List (n,list1,x,(fun n x -> nextUntilCorrectList p y n x))
      | _ -> failwith "filter"

    let partitioned_map p f gen = match gen with
      |Const a when p a -> Const (fst f a)
      |Const a -> Const (snd f a)
      |Arg (x,y) when p (y x) -> Arg (x,(fun x -> fst f (y x)))
      |Arg (x,y) -> Arg (x,(fun x -> snd f (y x)))
      |List(n,list1,x,y) when p (y n x) -> Arg (x,(fun x -> fst f (y n x)))
      |List (n,list1,x,y) -> Arg(x,(fun x -> snd f (y n x)))
      | _ -> failwith "partitioned_map"

    
    
  end ;;
  
  
  
  
(*============================================================================================*)
(*======================================      TEST      ======================================*)
(*============================================================================================*)


  let  a = Generator.const "Carabistouille" in 
  let c = Generator.list 3 a in 
  Generator.next c;;
  
  let  a = Generator.bool 0.01 in 
  let  b = Generator.list 100 a in 
  let c = Generator.list 3 b in 
  Generator.next c;;

  let  a = Generator.int 2 10 in 
  let  b = Generator.list 10 a in 
  let c = Generator.list 3 b in 
  Generator.next c;;
  
  
  let charG= Generator.alphanum in
  let  a = Generator.string 10 charG in 
  let  b = Generator.list 10 a in 
  let c = Generator.list 3 b in 
  Generator.next c;;
  
  let floatG = Generator.float 1.4 2.5 in
  let  b = Generator.list 10 floatG in 
  let c = Generator.list 3 b in 
  Generator.next c;;
  
  let graph = Generator.graph 10 10 in 
  Generator.next graph;;

  let  a = Generator.int 2 10 in 
        let x = Generator.combine  (Generator.list 2 a) (Generator.list 5 a) in Generator.next x;;
	
  let x = Generator.combine (Generator.const "test") (Generator.int 2 10) in Generator.next x;;
  let x1 = Generator.combine (Generator.string 10 Generator.alphanum) (Generator.float 2.4 5.2) in Generator.next x1;;


  let x2 = Generator.map (fun x -> x*10) (Generator.int 2 10) in Generator.next x2;;
  let x4 = "------";;
  let x3 = Generator.filter (fun x -> x mod 2 = 0) (Generator.int 1 40) in Generator.next x3;;
  (*Filter a list of int*)
  let x5 = Generator.filter (fun l -> List.for_all (fun x -> x mod 2 = 0) l) (Generator.list 10 (Generator.int 1 40)) in Generator.next x5;;
  let x4 = "------";;

  let x4 = Generator.partitioned_map (fun x -> x mod 2 = 0) ((fun x -> x*10), (fun x -> x*1000)) (Generator.int 1 40) in Generator.next x4;;

  let x6 = Generator.partitioned_map (fun l -> List.for_all (fun x -> x mod 2 = 0) l) ((fun l -> List.map (fun x -> x) l), (fun l -> List.map (fun x -> x*1000) l)) (Generator.list 10 (Generator.int 1 40)) in Generator.next x6;;
  let x7 = Generator.map (fun l -> List.map (fun x -> x * 1000) l) (Generator.list 10 (Generator.int 1 40)) in let x8 = Generator.next x7 in (x8, Generator.next x7);;
