module Reduction :
  sig
    (** Type d'une stratégie de réduction des éléments de type 'a
      * Une stratégie associe à chaque valeur une liste de propositions plus "simples".
      * NB : Les propositions sont ordonnées dans l'ordre croissance de "simplicité"
      *      (i.e. les valeurs les plus "simples" sont en début de liste).
      * IMPORTANT : Les stratégies implémentées respectent les conditions des générateurs correspondants.
      *)
    type 'a t = 'a -> 'a list

    (** La stratégie vide : ne renvoie aucune proposition de réduction *)
    val empty : 'a t

    (* TYPES DE BASE *)

    (** Stratégie de réduction sur les entiers
      * @param n entier
      * @return  liste d'entiers plus "simples" entre `-|n|` et `|n|`
      *)
    val int : int t

    (** Stratégie de réduction sur les entiers positifs
      * @param n entier positif
      * @return  liste d'entiers naturels plus "simples" entre 0 et `n`
      *)
    val int_nonneg : int t

    (** Stratégie de réduction sur les flottants
      * @param x flottant
      * @return  liste de flottants plus "simples" entre `-|x|` et `|x|`
      *)
    val float : float t

    (** Stratégie de réduction sur les flottants positifs
      * @param x flottant positif
      * @return  liste de flottants positifs plus "simples" entre `0` et `x`
      *)
    val float_nonneg : float t

    (** Stratégie de réduction sur les caractères
      * @param c caractère
      * @return  liste de caractères plus "simples"
      *)
    val char : char t

    (** Stratégie de réduction sur les caractères alphanumériques
      * @param c caractère alphanumérique
      * @return  liste de caractères alphanumériques plus "simples"
      *)
    val alphanum : char t

    (* CHAINES DE CARACTERES *)

    (** Stratégie de réduction sur les chaînes de caractères
      * @param red stratégie de réduction à utiliser sur chaque caractère
      * @param s   chaîne de caractères
      * @return    liste de chaînes de caractères plus "simples" au pire aussi longues que `s`
      *)
    val string : char t -> string t

    (* LISTES *)

    (** Stratégie de réduction sur les listes
      * @param red stratégie de réduction à utiliser sur chaque élément
      * @param l   liste
      * @return    liste de listes plus "simples" au pire aussi longues que `l`
      *)
    val list : 'a t -> ('a list) t

    (* GRAPHES *)
    (** Stratégie de réduction sur les graphes
      * @param red stratégie de réduction à utiliser sur chaque élément
      * @param g   graphe
      * @return    liste de graphes plus "simples" au pire aussi grands que `g`
      *)
    val graph : (int * int * int) list -> (int * int * int) list list

    (* TRANSFORMATIONS *)

    (** Stratégie de réduction sur les couples
      * @param fst_red stratégie de réduction de la première coordonnée
      * @param snd_red stratégie de réduction de la deuxième coordonnée
      * @return        stratégie de réduction sur les couples correspondants
      *)
    val combine : 'a t -> 'b t -> ('a * 'b) t

    (** Applique un filtre à une stratégie de réduction
      * @param p   filtre à appliquer à chaque réduction
      * @param red stratégie de réduction
      * @return    stratégie de réduction ne contenant que des propositions vérifiant `p`
      *)
    val filter : ('a -> bool) -> 'a t -> 'a t
  end =
  struct
    type 'a t = 'a -> 'a list

    (* TODO : Implémenter tous les éléments de la signature manquants *)


    (*pas de stratégie : liste vide*)
    let empty a = [] 

    let rec supprimer_doublons l =
      match l with
         [] -> []
        |h::t -> if (List.mem h t) then supprimer_doublons t else h::(supprimer_doublons t)

    (*============================================================================================================*)
    (*======================================            Entiers             ======================================*)
    (*============================================================================================================*)

    let rec trouver_multiples_inferieurs_int_pos n multiple div l = 
      if multiple > n then l
      else trouver_multiples_inferieurs_int_pos n (multiple + div) div (l @ [multiple])

    let rec trouver_multiples_superieurs_int_neg n multiple div l = 
      if multiple < n then l
      else trouver_multiples_superieurs_int_neg n (multiple - div) div (l @ [multiple])

    (*Renvoie la longueur du nombre x*)
    let length_number x = let s = string_of_int x in String.length s  
    
    let int_nonneg n =
      let longueur_n = length_number n in
          let multiple = Float.to_int (10. ** (Float.of_int (longueur_n - 1))) in 
          trouver_multiples_inferieurs_int_pos n 0 multiple []

    (* 
       Pour un entier, on utilise la longeur du nombre pour renvoyer les nombres plus ou moins précis.
       Pour un nombre de longueur n par exemple, on renverra les multiples de 10**(n-1) dont leurs valeurs absolues lui sont inférieurs.
    *)
    let int n = 
      if n > 0 then 
        int_nonneg n
      else
        let longueur_n = length_number n - 1 in
          let multiple = Float.to_int (10. ** (Float.of_int (longueur_n - 1))) in 
            trouver_multiples_superieurs_int_neg n 0 multiple []


    (*============================================================================================================*)
    (*======================================            Floatant            ======================================*)
    (*============================================================================================================*)


    let rec trouver_multiples_inferieurs_float_pos x multiple div l = 
      if multiple > x then l
      else trouver_multiples_inferieurs_float_pos x (multiple +. div) div (l @ [Float.of_string (Float.to_string multiple)])

    let rec trouver_multiples_superieurs_float_neg x multiple div l = 
      if multiple < x then l
      else trouver_multiples_superieurs_float_neg x (multiple -. div) div (l @ [Float.of_string (Float.to_string multiple)])

    (*Renvoie la precision du float entré en parametre. Ex : precision 1.001 = 3 *)
    let precision x = let s = string_of_float x in
      let rec aux i = if s.[i] = '.' then i else aux (i+1) in
      String.length s - aux 0 - 1 

    let float_nonneg x = 
      trouver_multiples_inferieurs_float_pos x 0. (1. /. 10. ** (Float.of_int (precision x) -. 1.)) []

    (* 
       Pour un floatant, on utilise la précision du nombre pour renvoyer les nombres plus ou moins précis.
       Pour un nombre de précision x par exemple, on renverra les multiples de (1/ (10**(x-1))) dont leurs valeurs absolues lui sont inférieurs.
    *)
    let float x = 
      if x > 0. then
        float_nonneg x
      else 
        trouver_multiples_superieurs_float_neg x 0. (1. /. 10. ** (Float.of_int (precision x) -. 1.)) []

(*==================================================================================================*)
(*======================================      Caractères      ======================================*)
(*==================================================================================================*)

let is_digit c =
  let code = Char.code c in
  code >= Char.code '0' && code <= Char.code '9'

let is_alpha c =
  let code = Char.code (Char.lowercase_ascii c) in
  code >= Char.code 'a' && code <= Char.code 'z'

let char c =
  let upper = Char.uppercase_ascii c in
  let lower = Char.lowercase_ascii c in
  if c = lower then [upper]
  else if c = upper then [lower]
  else [lower; upper]

(** Stratégie de réduction sur les caractères alphanumériques
      * @param c caractère alphanumérique
      * @return  liste de caractères alphanumériques plus "simples"
      *)
let alphanum c =
  if is_digit c then
    let prev = if c = '0' then '\000' else Char.chr (Char.code c - 1) in
    let next = if c = '9' then '\000' else Char.chr (Char.code c + 1) in
    [prev; next]
  else if is_alpha c then
    let upper = Char.uppercase_ascii c in
    let lower = Char.lowercase_ascii c in
    if c = lower then [upper]
    else if c = upper then [lower]
    else [upper; lower]
  else
    []


(*============================================================================================================*)
(*======================================      Chaine de caractères      ======================================*)
(*============================================================================================================*)
  (* CHAINES DE CARACTERES *)

    (** Stratégie de réduction sur les chaînes de caractères
      * @param red stratégie de réduction à utiliser sur chaque caractère
      * @param s   chaîne de caractères
      * @return    liste de chaînes de caractères plus "simples" au pire aussi longues que `s`
    *)

   


(* Applique la stratégie de réduction de manière récursive *)
let rec apply_reduction red s index partial acc max_results =
  if index = String.length s then
    if partial = s then (acc, false)
    else
      let new_acc = partial :: acc in
      if List.length new_acc >= max_results then (new_acc, true) else (new_acc, false)
  else
    let char_list = red s.[index] in
    let string_list = List.map (String.make 1) (List.filter ((<>) '\000') char_list) in
    let folder (acc, stop) str =
      if stop then (acc, stop)
      else
        let (new_acc, stop) = apply_reduction red s (index + 1) (partial ^ str) acc max_results in
        if stop then (new_acc, stop)
        else apply_reduction red s (index + 1) partial new_acc max_results
    in
    List.fold_left folder (acc, false) string_list

(* Stratégie de réduction sur les chaînes de caractères *)
let string red s =
  if String.length s = 0 then []
  else 
    let (unsorted_result, _) = apply_reduction red s 0 "" [] 10000 in
    let result_without_empty = List.filter (fun s -> s <> "") unsorted_result in
    supprimer_doublons (List.sort (fun a b -> compare (String.length a) (String.length b)) result_without_empty)



(*==============================================================================================*)
(*======================================      Listes      ======================================*)
(*==============================================================================================*)
 

   (** Stratégie de réduction sur les listes
    * @param red stratégie de réduction à utiliser sur chaque élément
    * @param l   liste
    * @return    liste de listes plus "simples" au pire aussi longues que `l`
    *)
    let list red l =
      let rec liste_reduite red l =
        match l with
        | [] -> []
        | x::xs -> (red x)::(liste_reduite red xs)
      in liste_reduite red l


(*==============================================================================================*)
(*======================================      Graphes     ======================================*)
(*==============================================================================================*)

(*Fonction pour debug. Affiche un graphe (appeler la fonction avant de lancer une fonction de réduction*)
let rec print_list = function 
[] -> ()
| (e,f,g)::l -> print_int e ; print_string ","; print_int f; print_string " " ; print_list l 

(*Inverse quelque arêtes aléatoirement*)
let rec invert_some_edges graph = match graph with
	|[] ->[]
	|(u,v,w)::xs when Random.int 2 = 1 -> (v,u,w)::(invert_some_edges xs)
	|(u,v,w)::xs ->(u,v,w)::(invert_some_edges xs) 

(*Inverse quelque arêtes aléatoirement des graphs dans une liste de graph*)
let rec invert_list_of_graphs graphs res = match graphs with
|[] ->res
|graph::xs ->  invert_list_of_graphs xs ((invert_some_edges graph)::res)

(* Fonction pour supprimer les noeuds isolés *)
let remove_isolated_nodes graph =
  let nodes = List.fold_left (fun acc (u, v, _) -> u :: v :: acc) [] graph |> List.sort_uniq compare in
  List.filter (fun (u, v, _) ->
    let neighbors_u = List.exists (fun (s, d, _) -> u = d || u = s) graph in
    let neighbors_v = List.exists (fun (s, d, _) -> v = d || v = s) graph in
    neighbors_u && neighbors_v) graph

(* Supprime les boucles *)
let remove_loops graph =
  List.filter (fun (u, v, _) -> u <> v) graph

(* Supprime les arêtes avec une valeur inférieure à un seuil *)
let remove_edges_below_threshold graph threshold =
  List.filter (fun (_, _, w) -> w >= threshold) graph

(*Supprime les aretes avec la même origine et la même destination*)
let remove_duplicate_edges graph =
  let rec remove_duplicate_edges_aux graph acc =
    match graph with
    | [] -> acc
    | (u, v, w)::xs ->
      let new_graph = List.filter (fun (s, d, _) -> not (u = s && v = d)) xs in
      remove_duplicate_edges_aux new_graph ((u, v, w)::acc)
  in remove_duplicate_edges_aux graph []
 
(*Cherche les voisins d'un sommet u*)
let rec find_neighbours graph u res = match graph with
|[]->res
|(x,v,w)::xs when x=u && not (List.mem v res) -> find_neighbours xs u (v::res)
|(x,v,w)::xs ->find_neighbours xs u res

(*Cherche les composantes de connexe en partant du sommet u*)
let rec find_connexe_u graph u neighbours already_search_through = match neighbours with
|[]-> already_search_through
|v::xs when List.mem v already_search_through -> find_connexe_u graph u xs already_search_through
|v::xs  ->  find_connexe_u graph u (xs @ (List.fold_left (fun acc x -> if (List.mem x neighbours) then acc else x::acc ) neighbours (find_neighbours graph v []))) (v::already_search_through)

let rec connexe graph graph_used res already_seen = match graph_used with
|[]->res
|(u,_,_)::xs when List.mem u already_seen ->  connexe graph xs res already_seen
|(u,_,_)::xs -> connexe graph xs ((find_connexe_u graph u  (find_neighbours graph u []) [u])::res) (u::already_seen)

(*Construit le graph associé à une composante connexe*)
let rec build_connex_part_of_graph graph connex res = match graph with
|[] -> res
|(u,v,w)::xs when List.mem u connex -> build_connex_part_of_graph xs connex ((u,v,w)::res)
|(u,v,w)::xs -> build_connex_part_of_graph xs connex res


(*Forment les graphes de chaque composantes connexes*)
let rec list_connex_part_of_graph graph list_connex res= match list_connex with
|[]->res
|connex::xs -> list_connex_part_of_graph graph xs ((build_connex_part_of_graph graph connex [])::res)

(*Reduit une liste de graphs*)
let rec reduced_from_list_connex_part_of_graph g res= match g with
|[] -> res
|connex_graph::xs -> reduced_from_list_connex_part_of_graph xs (remove_loops (remove_duplicate_edges (remove_isolated_nodes connex_graph))::res) 

(*Reduit les graphs des composantes connexes d'un graph, et ajoute des graphs dont o a modifier des arêtes aléatoirement, et ce n fois*)
let rec reduce_graph1 graph n res= match n with
|0 -> res
|n -> let graphs =  reduced_from_list_connex_part_of_graph (list_connex_part_of_graph graph (connexe graph graph [] []) []) [] in reduce_graph1 graph (n-1) (graphs @ (invert_list_of_graphs graphs []) @res) 


(*Efface les graphs doublons dans res*)
let clean_reduce_graph res =
	List.fold_left(fun acc x -> if List.mem x acc then acc else x :: acc) [] res 

(*Reduit le graph *)
let reduce_graph graph = clean_reduce_graph ( reduce_graph1 graph 10 [])

(* Stratégie de réduction sur les graphes *)
let graph g = reduce_graph g

(*=======================================================================================================*)
(*======================================      Transformations      ======================================*)
(*=======================================================================================================*)

    (** Stratégie de réduction sur les couples
      * @param fst_red stratégie de réduction de la première coordonnée
      * @param snd_red stratégie de réduction de la deuxième coordonnée
      * @return        stratégie de réduction sur les couples correspondants
      *)

    let combine fst_red snd_red =
      fun (x,y) -> List.map (fun x -> (x,y)) (fst_red x) @ List.map (fun y -> (x,y)) (snd_red y)

    (** Stratégie de réduction sur les couples
      * @param red stratégie de réduction à utiliser sur chaque élément
      * @return    stratégie de réduction sur les couples correspondants
      *)


    (** Applique un filtre à une stratégie de réduction
      * @param p   filtre à appliquer à chaque réduction
      * @param red stratégie de réduction
      * @return    stratégie de réduction ne contenant que des propositions vérifiant `p`
      *)
    (*val filter : ('a -> bool) -> 'a t -> 'a t*)
    let filter p red =
      fun x -> List.filter p (red x)

  
  end ;;
