#use "Test.ml" ;;

(* Tests de la division euclidienne                                                                          *)
(* Les tests sont effectués sur des couples d'entiers compris entre -100 et 100 dont le second est *non nul* *)
(* (d'où l'utilisation du filtre pour éviter des divisions par zéro).                                        *)
let gen_intcouple =
  let gen_dividend =                            Generator.int (-100) 100
  and gen_divisor  = Generator.filter ((<>) 0) (Generator.int (-100) 100)
    in Generator.combine gen_dividend gen_divisor ;;
let red_intcouple =
  let red_dividend =                           Reduction.int
  and red_divisor  = Reduction.filter ((<>) 0) Reduction.int
    in Reduction.combine red_dividend red_divisor ;;
let test_intcouple = Test.make_test gen_intcouple red_intcouple ;;

(* Construction des tests *)
let test_quorem       = test_intcouple "/ et mod (correct)" (fun (a, b) -> (a = (a / b) * b + (a mod b))) ;;
let test_quorem_wrong = test_intcouple "/ et mod (faux)"    (fun (a, b) -> (a = (a / b) * b - (a mod b))) ;;

(* Exécution des tests *)
Test.check    100 test_quorem       ;;
Test.check    100 test_quorem_wrong ;;
Test.fails_at 100 test_quorem       ;;
Test.fails_at 100 test_quorem_wrong ;;
Test.execute  100 [test_quorem ; test_quorem_wrong] ;;

(* Tests sur la concaténation de listes                                           *)
(* Les tests sont effectués sur des listes d'au plus dix entiers entre 0 et 10. *)
let gen_intlistcouple =
  let gen_intlist = Generator.list 10 (Generator.int_nonneg 10) in
    Generator.combine gen_intlist gen_intlist ;;
let red_intlistcouple =
  let red_intlist = Reduction.list     Reduction.int_nonneg     in
    Reduction.combine red_intlist red_intlist ;;
let test_intlistcouple = Test.make_test gen_intlistcouple red_intlistcouple ;;

(* Constructon des tests *)
let test_append       = test_intlistcouple "List.@ (correct)" (fun (l1, l2) -> List.length (l1 @ l2) = (List.length l1) + (List.length l2)) ;;
let test_append_wrong = test_intlistcouple "List.@ (faux)"    (fun (l1, l2) -> List.length (l1 @ l2) = (List.length l1) - (List.length l2)) ;;

(* Exécution des tests *)
Test.check    100 test_append       ;;
Test.check    100 test_append_wrong ;;
Test.fails_at 100 test_append       ;;
Test.fails_at 100 test_append_wrong ;;
Test.execute  100 [test_append ; test_append_wrong] ;;



(* Tests sur les degrés d'un sommet dans un graph                                         *)
(* Les tests sont effectués sur des graphs d'au plus dix sommets avec au plus 20 arêtes *)

let gen_graph = Generator.graph 10 20;;
let red_graph = Reduction.graph  ;;
let test_graph = Test.make_test gen_graph red_graph ;;

let rec degree graph u res= match graph with
|[]->res
|(v,_,_)::xs when u=v -> degree xs u (res+1)
|(v,_,_)::xs -> degree xs u res;;

let rec somme_degree graph graph_copy already_seen = match graph_copy with
|[] -> 0
|(u,v,w)::xs when List.mem u already_seen -> somme_degree graph xs already_seen
|(u,v,w)::xs -> (degree graph u 0)+ somme_degree graph xs (u::already_seen);;

(* Constructon des tests *)
let test_append       = test_graph "graph.@ (correct)"   (fun (g) ->(List.length g)= (somme_degree g g []));;
let test_append_wrong = test_graph "graph.@ (faux)"    (fun (g) -> (List.length g) +1 = (somme_degree g g []))  ;;

(* Exécution des tests *)
Test.check    100 test_append       ;;
Test.check    100 test_append_wrong ;;
Test.fails_at 100 test_append       ;;
Test.fails_at 100 test_append_wrong ;;
Test.execute  100 [test_append ; test_append_wrong] ;;
